#!/bin/bash
#Скрипт по пакетной установки программ с возможностью их выбора 
#Автор: Александр Клич, сайт https://prostolinux.my1.ru
#"Спасибо klichalex автору скрипта Transformation! https://www.youtube.com/user/klichalex/featured"
#"Спасибо klichalex автору хороших видео уроков! https://rutube.ru/channel/23628980/videos/"
#"Спасибо klichalex автору перевода Refracta на Русский язык http://prostolinux.my1.ru/"
#Дополнение chebur chebur3133@gmail.com https://prostolinux.my1.ru/index/st/0-11 https://gitlab.com/Chebur70"
#"Спасибо circulosmeos https://github.com/circulosmeos за предоставленые скрипты"
#"Спасибо разработчику скриптов Dmitriy wj42ftns Chekhov, Russia https://gist.github.com/wj42ftns"
#"Спасибо kachnu https://github.com/kachnu за предоставленые скрипты"
#"Спасибо Kendek https://gitlab.com/Kendek/systemback"
#"Спасибо LeCorbeau's Vault https://lecorbeausvault.wordpress.com/2021/01/10/quickly-build-a-custom-bootable-installable-debian-live-iso-with-live-build/"
#"Спасибо Франко Кониди https://syslinuxos.com/things-to-do-after-installing-syslinuxos-12/"
#"Не важно насколько медленно ты движешься, главное не останавливаться.Конфуций"
#"Наш ответ Чемберлену!Дави Империализма Гиену Могучий Рабочий Класс! Вчера были танки лишь у Чемберлена,А нынче есть и у нас!"
#"Запомните,чтобы ничего не делать, надо уметь делать все" 
#"Я не потерпел неудачу, я нашел 10 000 способов, которые не сработают-Эдисон."

cd ~

#Добавление архитектуры i386
sudo dpkg --add-architecture i386 
sudo apt update && sudo apt upgrade -y
sudo apt autoremove -y 
#Установка обязательных программ
sudo apt install -y sudo curl wget apt-transport-https gdebi dirmngr aptitude engrampa onboard unzip libfuse2 
#Удаление старых настроек 
rm -Rf ~/.config/xfce4/
#Удоление папки autostart
rm -Rf ~/.config/autostart/
#Добавление папки autostart
mkdir ~/.config/autostart 
#Добавление папки applications
mkdir ~/.local/share/applications 
#Добавление папки Uninstall
mkdir ~/'Рабочий стол'/Uninstall
#Настройка теминала
rm -rf .bashrc
wget https://www.dropbox.com/sh/wzzgx3j35lziyhz/AABapMGhtZVW3KvRec8gcbB2a?dl=0 && \
unzip -q 'AABapMGhtZVW3KvRec8gcbB2a?dl=0'
tar xvf Appearance/bashrc.tar.gz
#Appearance
tar xvf Appearance/appearance.tar.gz
chmod -R 777 appearance
#Applications
tar xvf Appearance/applications.tar.gz
chmod -R 777 applications
cp -R applications ~/.local/share/ && rm -Rf applications
#Icons
tar xvf Appearance/icons.tar.gz
chmod -R 777 icons
sudo cp -R icons /usr/share/ && rm -Rf icons
#Themes
tar xvf Appearance/themes.tar.gz
chmod -R 777 themes
sudo cp -R themes /usr/share/ && rm -Rf themes
#Backgrounds
tar xvf Appearance/backgrounds.tar.gz
chmod -R 777 backgrounds
sudo cp backgrounds/05.jpg /boot/grub
sudo update-grub
sudo cp -R backgrounds /usr/share/ && rm -Rf backgrounds
#Install gdebi от Klichalex
tar xvf Appearance/gdebi.desktop.tar.gz
sudo mv gdebi.desktop /usr/share/applications/		
rm -rf 'AABapMGhtZVW3KvRec8gcbB2a?dl=0' && rm -Rf Appearance
# Установка программ по выбору.
sudo apt install -y dialog
echo "================================="
echo "ВАШ БЕСПРОВОДНОЙ КОНТРОЛЕР,запомните его."
echo "(Если строка ниже пустая, то его нет)"
lspci | grep -i Network
echo "================================="
sleep 13s

cmd=(dialog --separate-output --checklist "Выберите программное обеспечение, которое вы хотите установить:" 22 76 16)
	options=(1 "Grub themes Rosa" on  #любой параметр может быть установлен по умолчанию на "off"
		  2 "Plymout themes Percentage" off
		  3 "QtFsarchiver-утилита для клонирования диска под Linux" off
		  4 "BackUpRestore-7.0-системы Linux" off
		  5 "Systemback-Резервное копирование" off
		  6 "Slimjet-быстрый и безопасный веб-браузер" off
		  7 "PaleMoon-браузер" off
		  8 "GoogleChrome-stable-браузер" off
		  9 "Chromium-браузер" off
		10 "Opera-stable-браузер" off
		11 "Vivaldi-браузер" off
		12 "Minbrowser-браузер" off
		13 "Brave-браузер с открытым исходным кодом" off
		14 "Torbrowser-мощный инструмент для защиты приватности и онлайновых свобод" off
		15 "LibreWolf-Браузер, фокусирующийся на безопасности" off
		16 "Gvidm-утилита для быстрого изменения разрешения экрана" on
		17 "Qshutdown-утилита для отключения компьютера" on
		18 "Bucklespring-консольная утилита для эмуляции "щелчков" механической клавиатуры " on
		19 "ThunarOfRoot-Работа с архивами в xfce4" on
		20 "CompizEmerald-композитный менеджер" off
		21 "Stacer-Оптимизация, очистка, настройка системы" off
		22 "BleachBit-инструмент,освободит диск ,обеспечив безопасность данных" off
		23 "GKrellM-виджет системных мониторов-Linux" off
		24 "SpeedtestNetWeb-скорость интернета и задержку соединения" off
		25 "GoogleTranslateWeb-позволяет мгновенно переводить слова" off
		26 "SkypeWeb-мессенджер" off
		27 "SMSMessagesWeb-сообщения на компьютере" off
		28 "TelegramWeb-мессенджер" off
		29 "Signal-мессенджер" off
		30 "WireWeb-приложение для зашифрованной связи" off
		31 "WhatsAppWeb-мессенджер" off
		32 "ViberAppimage-мессенджер" off
		33 "Cherrytree-Приложение для создания иерархических заметок" on
		34 "Gotop-монитор графической активности на основе терминалов" off
		35 "Anydesk-удаленная помощь" off
		36 "Galculator-научный калькулятор" off
		37 "BauhAppimage-магазин приложений AppImage, AUR, Flatpaks и Snaps для Linux" off
		38 "LiveUSBMakerQtAppImageDDSh-создания загрузочных Live USB, копирования ISO-образов" off
		39 "Mintstick-Графический интерфейс для записи файлов .img или .iso на USB" off
		40 "Ventoy-создание мультизагрузочной флешки" off
		41 "MultiBootUSB-создания мультизагрузочных USB-носителей" off
		42 "UNetbootin-Утилита для создания Live USB" off
		43 "Etcher-предназначенное для записи файлов образов дисков" off
		44 "RosaImageWriter-Запись ISO-образов на USB-диск" off
		45 "ddCopy-создания загрузочных Live USB, копирования ISO-образов" off
		46 "Дополнительные драйвера Wi-Fi Marvell и NXP (Libertas)" off
		47 "Дополнительные драйвера Wi-Fi Intel" off
		48 "Дополнительные драйвера Wi-Fi Realtek" off
		49 "Дополнительные драйвера Wi-Fi Broadcom" off
		50 "Дополнительные драйвера Wi-Fi Atheros" off
		51 "Любая Ваша программа" off
		52 "Любая Ваша программа" off
		53 "Любая Ваша программа" off
		54 "Любая Ваша программа" off
		55 "Любая Ваша программа" off
		56 "Любая Ваша программа" off
		57 "Любая Ваша программа" off
		58 "Любая Ваша программа" off
		59 "Любая Ваша программа" off
		60 "Любая Ваша программа" off
		61 "Любая Ваша программа" off
		62 "Любая Ваша программа" off
		63 "Любая Ваша программа" off
		64 "Любая Ваша программа" off
		65 "Любая Ваша программа" off
		66 "Любая Ваша программа" off
		67 "Любая Ваша программа" off
		68 "Любая Ваша программа" off
		69 "Любая Ваша программа" off
		70 "Автоматические обновления безопасности" off
		71 "AppImageUpdate-обновляет на основе информации встроенной в AppImage" off
                72 "MOCP-Терминальный музыкальный плеер" off 
                73 "Transmission-простой BitTorrent-клиент c открытым исходным кодом" off
                74 "qBittorrent-бесплатный клиент для файлообменной сети BitTorrent" off
                75 "Persepolis-менеджер загрузок и графический интерфейс для Aria2" off
                76 "ClipGrabAppimage-это бесплатный загрузчик и конвертор видео из YouTube" off
                77 "XtremeDownloadManager-зaгрузчик файлов" off
                78 "YoutubeDL+Gui-Консольный+Графический интерфейс медиа-загрузчика" off
                79 "Yt-dlp-это очень хороший и мощный форк известной утилиты Youtube-dl" off
                80 "JDownloader-это бесплатный инструмент управления загрузками" off
                81 "MediaDownloader-Загружает видео" off
                82 "MotrixAppimage-загружайте большие файлы без сбоев" off
                83 "Любая Ваша программа" off
                84 "Любая Ваша программа" off
                85 "Pluma-легковерный текстовый редактор" off
                86 "Любая Ваша программа" off
                87 "FeatherPad-бесплатный текстовый редактор" off
                88 "Leafpad-текстовый редактор под Linux" off
                89 "Spedit-Простой текстовый редактор forked from Leafpad" off
                90 "Medit-это более быстрый аналог Gedit" on
                91 "Gedit-свободный текстовый редактор" off
                92 "OpenOfficeAppimage-свободный пакет офисных приложений, основан на коде StarOffice" off
                93 "FreeofficeAppimage-Бесплатный полнофункциональный офисный пакет" off
                94 "OnlyOfficeAppimage-офисный пакет с открытым исходным кодом" off
                95 "WPSOfficeAppimage-офисный пакет" off
                96 "Conky-мощный и легко настраиваемый системный монитор" off
                97 "GnomePie-лаунчер для быстрого запуска других программ" off
                98 "Plank-простая док-панель" off
                99 "Cairo-dock-простая док-панель" off
              100 "Речевой Сервер Speech Dispatcher" off
              101 "Из каталога deb-Любые debпакеты с зависимостями что закинете" off)

		choices=$("${cmd[@]}" "${options[@]}" 2>&1 >/dev/tty)
		clear
		for choice in $choices
		do
		    case $choice in
	        	      1)
	            		#Install GrubThemesRosa
				echo "================ Установка GrubThemesRosa ================"								 
                                wget https://www.dropbox.com/sh/pm6hq9npw8jahvw/AADwWMEDOrwkht9unmc9vExYa?dl=0 && \
				unzip -q 'AADwWMEDOrwkht9unmc9vExYa?dl=0'   
                                sudo mkdir /boot/grub/themes/    				       
				tar xvf RosaPercentage/rosa.tar.gz                                                   
                                sudo mv rosa /boot/grub/themes/rosa    				                                                                                                                                                                                                                                                    
                                tar xvf RosaPercentage/GrubRosa.tar.gz                                
                                sudo mv grub /etc/default/grub                                                          
                                sudo update-grub   
				cp RosaPercentage/GrubThemesRosaUninstall.sh ~/'Рабочий стол'/Uninstall
				rm -rf 'AADwWMEDOrwkht9unmc9vExYa?dl=0' && rm -Rf RosaPercentage                        
				;;
			     2)
				#Install PlymoutThemesPercentage
				echo "================ Установка PlymoutThemesPercentage ================"  
                                sudo apt install -y plymouth-themes    
				wget https://www.dropbox.com/sh/pm6hq9npw8jahvw/AADwWMEDOrwkht9unmc9vExYa?dl=0 && \
				unzip -q 'AADwWMEDOrwkht9unmc9vExYa?dl=0'
				tar xvf RosaPercentage/GrubRosa.tar.gz                                                                                                                                                                                                                                                                                                                                                   
                                sudo mv grub /etc/default/grub 
                                tar xvf RosaPercentage/percentage.tar.gz   			   			                                                          
                                sudo mv percentage /usr/share/plymouth/themes/percentage                                                                                       
                                sudo update-alternatives --install /usr/share/plymouth/themes/default.plymouth default.plymouth \
				/usr/share/plymouth/themes/percentage/percentage.plymouth 01
                                sudo plymouth-set-default-theme -R percentage
                                sudo update-initramfs -u -k all
                                sudo update-grub
				cp RosaPercentage/PlymoutThemesPercentageUninstall.sh ~/'Рабочий стол'/Uninstall
				rm -rf 'AADwWMEDOrwkht9unmc9vExYa?dl=0' && rm -Rf RosaPercentage                                                                                                                      
				;;
			     3)
                                #Install QtFsarchiverDeb
				echo "================ Установка QtFsarchiverDeb ================"
                                wget https://www.dropbox.com/sh/bi9txuzgd6r3ehr/AAAAlNIg-rGhJgC38L3yEBjza?dl=0 && \
				unzip -q 'AAAAlNIg-rGhJgC38L3yEBjza?dl=0'                             
                                sudo dpkg -i QtFsarchiver/Debian.deb
				sudo apt install -fy           
				chmod +x QtFsarchiver/qt-fsarchiver.desktop                                                         
                                cp QtFsarchiver/qt-fsarchiver.desktop ~/.local/share/applications/
                                sudo cp QtFsarchiver/QtFsarchiver.svg /usr/share/icons/                                                                    
				cp BackUpRestore/BackUp.png ~/'Рабочий стол'/
				cp BackUpRestore/Restore.png ~/'Рабочий стол'/
				cp QtFsarchiver/QtFsarchiverUninstall.sh ~/'Рабочий стол'/Uninstall 
                                rm -rf 'AAAAlNIg-rGhJgC38L3yEBjza?dl=0' && rm -Rf QtFsarchiver
				;;
			     4)
				#Install BackUpRestoreSh-7.0
			        echo "================ Установка BackUpRestoreSh-7.0 ================"
				wget https://www.dropbox.com/sh/wwmeaedbrimnbsl/AAAKupA10I71b5HilCdXsspIa?dl=0 && \
				unzip -q 'AAAKupA10I71b5HilCdXsspIa?dl=0'
				sudo dpkg -i BackUpRestore/gtkdialog_0.8.3-2mx21+2_amd64.deb
                                sudo apt install -fy
				sudo apt autoremove -y
                                sudo apt install -y git tar rsync wget gdisk openssl gzip bzip2 xz-utils pigz pbzip2 pixz
				chmod +x BackUpRestore/*.desktop				
				cp -R BackUpRestore/BackUpRestoreApl ~/.local/share/applications/
				chmod -R 777 BackUpRestore/BackUp
                                sudo cp -R BackUpRestore/BackUp /usr/share/icons/
				chmod +x BackUpRestore/BackUpRestore/*.sh
				sudo cp -R BackUpRestore/BackUpRestore /usr/local/bin/
				sudo chown -R $USER:$USER /usr/local/bin/BackUpRestore
				cp BackUpRestore/BackUp.png ~/'Рабочий стол'/
				cp BackUpRestore/Restore.png ~/'Рабочий стол'/
				cp BackUpRestore/BackUpRestoreUninstall.sh ~/'Рабочий стол'/Uninstall
			        rm -rf 'AAAKupA10I71b5HilCdXsspIa?dl=0' && rm -Rf BackUpRestore  
				;;
			     5)
				#Install Systemback
				echo "================ Установка Systemback ================"
				sudo mkdir /etc/apt/keyrings/				 
				sudo sh -c 'echo "deb [arch=amd64 signed-by=/etc/apt/keyrings/systemback.gpg] \
				http://mirrors.bwbot.org/ stable main" > /etc/apt/sources.list.d/systemback.list'
				sudo apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key 50B2C005A67B264F
                                sudo apt-key export A67B264F | sudo gpg --dearmour -o /etc/apt/keyrings/systemback.gpg
                                sudo apt-key del A67B264F
			        sudo apt update && sudo apt-get install -y systemback
				wget https://www.dropbox.com/sh/lea61fkqh0xd52g/AABqAEwpUr26hE1-DDgQpUYba?dl=0 && \
				unzip -q 'AABqAEwpUr26hE1-DDgQpUYba?dl=0'  
				sudo rm -rf /usr/share/systemback/lang/systemback_ru.qm
                                sudo cp Systemback/systemback_ru.qm  /usr/share/systemback/lang
				cp Systemback/SystembackUninstall.sh ~/'Рабочий стол'/Uninstall 
                                rm -rf 'AABqAEwpUr26hE1-DDgQpUYba?dl=0' && rm -Rf Systemback
				;;
			     6)
				#Install SlimjetDeb
			        echo "================ Установка SlimjetDeb ================"
                                wget https://www.dropbox.com/sh/cvwwvvvsdjh98jh/AACjiNduWLJJbgqqcHVSqPNpa?dl=0 && \
				unzip -q 'AACjiNduWLJJbgqqcHVSqPNpa?dl=0'
				sudo dpkg -i Slimjet/slimjet*.deb
                                sudo apt install -fy                  
				tar xvf Slimjet/slimjet.tar.gz    
                                mv slimjet ~/.config/slimjet
				cp Slimjet/SlimjetUninstall.sh ~/'Рабочий стол'/Uninstall 
                                rm -rf 'AACjiNduWLJJbgqqcHVSqPNpa?dl=0' && rm -Rf Slimjet                                                                                                 
				;;
			     7)
				#Install PaleMoonTar
				echo "================ Установка PaleMoonTar ================"
                                wget https://www.dropbox.com/sh/had6irovtbdciqq/AAAM-XymjB8FxiHQT-ILBfcZa?dl=0 && \
				unzip -q 'AAAM-XymjB8FxiHQT-ILBfcZa?dl=0'
				tar xvf PaleMoon/palemoon*.tar.xz
				sudo mv palemoon /opt/palemoon
				sudo chown -R $USER:$USER /opt/palemoon
                                chmod +x PaleMoon/PaleMoonTar.desktop
                                cp PaleMoon/PaleMoonTar.desktop ~/.local/share/applications/
                                sudo cp PaleMoon/Palemoon.png /usr/share/icons/
				tar xvf PaleMoon/MoonchildProductions.tar.gz
                                chmod u+x '.moonchild productions'
				cp PaleMoon/PaleMoonUninstall.sh ~/'Рабочий стол'/Uninstall 
                                rm -rf 'AAAM-XymjB8FxiHQT-ILBfcZa?dl=0' && rm -Rf PaleMoon	 
				;;
			     8) 
				#Install GoogleChromeStable 
		                echo "================ Установка GoogleChromeStable ================"
				wget -q -O - https://dl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
                                sudo sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" \
				>> /etc/apt/sources.list.d/google.list'
                                sudo apt update && sudo apt install -y google-chrome-stable 				  		 
				wget https://www.dropbox.com/sh/0wi9h2a7ej64xz7/AAAQNJGMkPEt1huEQJ9esjjPa?dl=0 && \
				unzip -q 'AAAQNJGMkPEt1huEQJ9esjjPa?dl=0'      				 
				chmod +x GoogleChrome/google-chrome.desktop                                                                                                                
                                cp GoogleChrome/google-chrome.desktop ~/.local/share/applications/
                                sudo cp GoogleChrome/GoogleChrome.png /usr/share/icons/
                                tar xvf GoogleChrome/google-chrome.tar.gz 
                                mv google-chrome ~/.config/google-chrome								                             
                               # echo 'Очистка дублей репозитериев (Chrome - создаёт дубли)'  
                                printf 'yes\n' | sudo python3 -OEs GoogleChrome/aptsources-cleanup.pyz 
				cp GoogleChrome/GoogleChromeStableUninstall.sh ~/'Рабочий стол'/Uninstall
				rm -rf 'AAAQNJGMkPEt1huEQJ9esjjPa?dl=0' && rm -Rf GoogleChrome
				rm  ~/.local/share/keyrings/login.keyring
				rm  ~/.local/share/keyrings/user.keystore         
				;;
			     9)
				#Install Chromium
				echo "================ Установка Chromium ================"
				sudo apt install -y chromium chromium-l10n chromium-sandbox                                 
				wget https://www.dropbox.com/sh/qkm84kodwizr3q4/AAB2tzdNJ4g7oB_Vu68Zy-1Wa?dl=0 && \
				unzip -q 'AAB2tzdNJ4g7oB_Vu68Zy-1Wa?dl=0'
                                chmod +x Chromium/chromium.desktop
                                cp Chromium/chromium.desktop ~/.local/share/applications/
                                sudo cp Chromium/Chromium.png /usr/share/icons/ 
                                tar xvf Chromium/chromium.tar.gz   
                                mv chromium ~/.config/chromium   
				cp Chromium/ChromiumUninstall.sh ~/'Рабочий стол'/Uninstall                       
                                rm -rf 'AAB2tzdNJ4g7oB_Vu68Zy-1Wa?dl=0' && rm -Rf Chromium                                           
				;; 
			   10) 
				#Install OperaDeb 
		                echo "================ Установка Opera ================"
                                wget https://www.dropbox.com/sh/f49m4vq27ytehz4/AACfwkqNO6CcizvhJElUIsqIa?dl=0 && \
				unzip -q 'AACfwkqNO6CcizvhJElUIsqIa?dl=0'
				sudo dpkg -i Opera/opera*.deb
                                sudo apt install -fy				
                                chmod +x Opera/opera.desktop
                                cp Opera/opera.desktop ~/.local/share/applications/ 
                                sudo cp Opera/Opera.svg /usr/share/icons/      
                                tar xvf Opera/opera.tar.gz    
                                mv opera ~/.config/opera     
				cp Opera/OperaUninstall.sh ~/'Рабочий стол'/Uninstall                  
                                rm -rf 'AACfwkqNO6CcizvhJElUIsqIa?dl=0' && rm -Rf Opera                                
				;;
			   11)
				#Install VivaldiSh          
                                echo "================ Установка VivaldiSh ================"                                                   
				wget https://www.dropbox.com/sh/2en0ti85y9mr4zc/AADXX_KNMM9t-vQnl-Gd_ewMa?dl=0 && \
				unzip -q 'AADXX_KNMM9t-vQnl-Gd_ewMa?dl=0'				
				tar xvf Vivaldi/vivaldi-snapshot.tar.gz
				mv vivaldi-snapshot .config/vivaldi-snapshot 
				chmod +x Vivaldi/install-vivaldi-snapshot_new.sh
                                sh Vivaldi/install-vivaldi-snapshot_new.sh	
				cp Vivaldi/VivaldiUninstall.sh ~/'Рабочий стол'/Uninstall 					
				rm -rf 'AADXX_KNMM9t-vQnl-Gd_ewMa?dl=0' && rm -Rf Vivaldi                                 
				;;
			   12)
                                #Install MinBrowserDeb
				echo "================ Установка MinBrowserDeb ================"
                                wget https://www.dropbox.com/sh/5179t2mpm71b685/AADn5KYTquvcyQf_o-OAlHQNa?dl=0 && \
				unzip -q 'AADn5KYTquvcyQf_o-OAlHQNa?dl=0'
                                sudo dpkg -i MinAplPng/min*.deb
				sudo apt install -fy  
				tar xvf MinAplPng/Min.tar.gz
				mv Min ~/.config/Min
				cp MinAplPng/MinUninstall.sh ~/'Рабочий стол'/Uninstall 
                                rm -rf 'AADn5KYTquvcyQf_o-OAlHQNa?dl=0' && rm -Rf MinAplPng		 
				;;
			   13)
				#Install BraveBrowserApt
		                echo "================ Установка BraveBrowserApt  ================"
                                sudo apt install -y curl
                                sudo curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg \
				https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg
				echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg] \
				https://brave-browser-apt-release.s3.brave.com/ stable main"\
				|sudo tee /etc/apt/sources.list.d/brave-browser-release.list
				sudo apt update && sudo apt install -y brave-browser
				wget https://www.dropbox.com/sh/6g531ht3q5nt9hd/AAAh45XaZU9LhSqLTojHkWkRa?dl=0 && \
				unzip -q 'AAAh45XaZU9LhSqLTojHkWkRa?dl=0' 
				tar xvf Brave/BraveSoftware.tar.gz
				mv BraveSoftware ~/.config/BraveSoftware
				cp Brave/BraveUninstall.sh ~/'Рабочий стол'/Uninstall 
                                rm -rf 'AAAh45XaZU9LhSqLTojHkWkRa?dl=0' && rm -Rf Brave
				;;
			   14)
				#Install TorBrowser
				echo "================ Установка TorBrowser ================"      
				wget https://www.dropbox.com/sh/3zfwfebyq47t7td/AAB8BZOjc6G66GYuODc6KCi8a?dl=0 && \
				unzip -q 'AAB8BZOjc6G66GYuODc6KCi8a?dl=0'		
				sudo dpkg -i Tor/tor-browser*.deb
				sudo apt install -fy
				cp Tor/TorBrowserUninstall.sh  ~/'Рабочий стол'/Uninstall			
				rm -rf 'AAB8BZOjc6G66GYuODc6KCi8a?dl=0' && rm -Rf Tor	 
				;;
			   15)
				#Install LibreWolfAppimage
				echo "================ Установка LibreWolfAppimage =============="  
                                wget https://www.dropbox.com/sh/xdh6sx560o105lg/AABIuMvChhQ2DrYk6PFJxrSUa?dl=0 && \
				unzip -q 'AABIuMvChhQ2DrYk6PFJxrSUa?dl=0'                                                                             
                                chmod +x LibreWolfApp/LibreWolfApp.desktop
                                cp LibreWolfApp/LibreWolfApp.desktop ~/.local/share/applications/
                                sudo mv LibreWolfApp/LibreWolf.png /usr/share/icons/ 				
                                chmod +x ./LibreWolfApp/*.AppImage
				sudo mv LibreWolfApp/LibreWolf*.AppImage /opt/LibreWolf.AppImage
				tar xvf LibreWolfApp/librewolf.tar.gz
				cp LibreWolfApp/LibreWolfUninstall.sh ~/'Рабочий стол'/Uninstall 
                                rm -rf 'AABIuMvChhQ2DrYk6PFJxrSUa?dl=0' && rm -Rf LibreWolfApp	                    
				;;
			   16)
				#Install Gvidm
			        echo "================ Gvidm ================"
				sudo apt install -y gvidm
				wget https://www.dropbox.com/sh/6nb2l797rk5mfhf/AACUxOh6ljJ_oy3vw_Yo6ahLa?dl=0 && \
				unzip -q 'AACUxOh6ljJ_oy3vw_Yo6ahLa?dl=0' 			 
                                chmod +x BGQ/gvidm.desktop
                                cp BGQ/gvidm.desktop ~/.local/share/applications/
				sudo cp BGQ/Gvidm.png /usr/share/icons/   
				cp BGQ/GvidmUninstall.sh ~/'Рабочий стол'/Uninstall			
				rm -rf 'AACUxOh6ljJ_oy3vw_Yo6ahLa?dl=0' && rm -Rf BGQ                                                                                                                                 
				;;
			   17)
				#Install Qshutdown
			        echo "================ Qshutdown ================"
				sudo apt install -y qshutdown
				wget https://www.dropbox.com/sh/6nb2l797rk5mfhf/AACUxOh6ljJ_oy3vw_Yo6ahLa?dl=0 && \
				unzip -q 'AACUxOh6ljJ_oy3vw_Yo6ahLa?dl=0' 			 
                                chmod +x BGQ/qshutdown.desktop
                                cp BGQ/qshutdown.desktop ~/.local/share/applications/
                                sudo cp BGQ/Qshutdown.png /usr/share/icons/
				cp BGQ/QshutdownUninstall.sh ~/'Рабочий стол'/Uninstall				
				rm -rf 'AACUxOh6ljJ_oy3vw_Yo6ahLa?dl=0' && rm -Rf BGQ  
				;;
			   18)
				#Install Bucklespring
			        echo "================ Bucklespring ================"
				sudo apt install -y bucklespring
				wget https://www.dropbox.com/sh/6nb2l797rk5mfhf/AACUxOh6ljJ_oy3vw_Yo6ahLa?dl=0 && \
				unzip -q 'AACUxOh6ljJ_oy3vw_Yo6ahLa?dl=0' 			 
                                chmod +x BGQ/Bucklespring.desktop
                                cp BGQ/Bucklespring.desktop ~/.local/share/applications/
				sudo cp BGQ/Bucklespring.png /usr/share/icons/   
				cp BGQ/BucklespringUninstall.sh ~/'Рабочий стол'/Uninstall				
				rm -rf 'AACUxOh6ljJ_oy3vw_Yo6ahLa?dl=0' && rm -Rf BGQ                               
				;;
			   19)
				#Install ThunarOfRoot-Работа с архивами в xfce4
			        echo "================ Установка ThunarOfRoot-Работа с архивами в xfce4 ================"
                                sudo apt install -y ark engrampa file-roller p7zip atool minizip zenity lzma pdlzip pbzip2 r-cran-zip rzip xarchiver tar \
				unar unrar-free thunar-archive-plugin ffmpeg  
				wget https://www.dropbox.com/sh/4j27qqksnmsq9wn/AADjG8vhqn64jId8QeazMOdRa?dl=0 && \
				unzip -q 'AADjG8vhqn64jId8QeazMOdRa?dl=0'
				sudo dpkg -i ThunarOfRoot/rar*.deb
				sudo apt install -fy
				tar xvf ThunarOfRoot/ThunarOfRoot.desktop.tar.gz                         
                                chmod +x ThunarOfRoot.desktop
                                mv ThunarOfRoot.desktop ~/.local/share/applications/ 
                                tar xvf ThunarOfRoot/ThunarGeany.tar.gz
                                cp -R Thunar ~/.config/ && rm -Rf Thunar
                                tar xvf ThunarOfRoot/Nyoutube2what.tar.gz   
				chmod +x youtube2what 
				sudo mv youtube2what /usr/local/bin/
                                tar xvf ThunarOfRoot/icons.tar.gz
                                chmod -R 777 icons
                                sudo cp -R icons /usr/share/ && rm -Rf icons
                                echo "===== GeanyApl ==================="
                                tar xvf ThunarOfRoot/geany.desktop.tar.gz
                                chmod +x geany.desktop
                                mv geany.desktop ~/.local/share/applications/   
                                cp ThunarOfRoot/Accept-AmamosLaVida.mp3 ~/'Рабочий стол'/
                                cp ThunarOfRoot/Nyoutube2what.png ~/'Рабочий стол'/ 
				cp ThunarOfRoot/ThunarOfRootUninstall.sh ~/'Рабочий стол'/Uninstall                            
                                rm -rf 'AADjG8vhqn64jId8QeazMOdRa?dl=0' && rm -Rf ThunarOfRoot                                                                                                                          				                                                                				 			                        
				;;
			   20)
				#Install CompizEmerald
			        echo "================ Установка CompizEmerald ================"
                                sudo apt install -y compiz compiz-core compiz-plugins compiz-plugins-default compiz-plugins-extra \
				compiz-plugins-main compizconfig-settings-manager emerald emerald-themes
		                wget https://www.dropbox.com/sh/9t72pow785wpceu/AAAScxcu5xmrmup57YfakUQsa?dl=0 && \
				unzip -q 'AAAScxcu5xmrmup57YfakUQsa?dl=0'
				chmod +x CompizEmeraldAplPng/*.desktop
				sudo cp CompizEmeraldAplPng/ccsm.desktop /usr/share/applications/
				sudo cp CompizEmeraldAplPng/compiz-start.desktop /usr/share/applications/
				sudo cp CompizEmeraldAplPng/emerald-theme-manager.desktop /usr/share/applications/
				chmod -R 777 CompizEmeraldAplPng/Compiz              
                                sudo cp -R CompizEmeraldAplPng/Compiz /usr/share/icons/  
				tar xvf CompizEmeraldAplPng/compiz.tar.gz  
                                mv compiz ~/.config/compiz      
				tar xvf CompizEmeraldAplPng/emerald.tar.gz
				chmod +x CompizEmeraldAplPng/CompizEmeraldApl/*.desktop				    				    
				cp -R CompizEmeraldAplPng/CompizEmeraldApl ~/.local/share/applications/
				cp CompizEmeraldAplPng/CompizEmeraldUninstall.sh ~/'Рабочий стол'/Uninstall
				rm -rf 'AAAScxcu5xmrmup57YfakUQsa?dl=0' && rm -Rf CompizEmeraldAplPng           
				;;
			  21)
				#Install Stacer
			        echo "================ Установка Stacer ================"
                                sudo apt install -y stacer       
				wget https://www.dropbox.com/sh/6db8pm1e530401b/AADO3fX5Jw1HNYt1WH9aAPJpa?dl=0 && \
				unzip -q 'AADO3fX5Jw1HNYt1WH9aAPJpa?dl=0'
				cp Bleachbit/StacerUninstall.sh ~/'Рабочий стол'/Uninstall   
                                rm -rf 'AADO3fX5Jw1HNYt1WH9aAPJpa?dl=0' && rm -Rf Bleachbit       
				;;
			  22)
                                #Install BleachBit
			        echo "================ Установка BleachBit ================"   
                                sudo apt install -y bleachbit  
                                wget https://www.dropbox.com/sh/6db8pm1e530401b/AADO3fX5Jw1HNYt1WH9aAPJpa?dl=0 && \
				unzip -q 'AADO3fX5Jw1HNYt1WH9aAPJpa?dl=0'
                                chmod +x Bleachbit/bleachbit-root.desktop
			        cp Bleachbit/bleachbit-root.desktop ~/.local/share/applications/
                                sudo cp Bleachbit/Bleachbit.png /usr/share/icons/
				cp Bleachbit/BleachBitUninstall.sh ~/'Рабочий стол'/Uninstall 
                                rm -rf 'AADO3fX5Jw1HNYt1WH9aAPJpa?dl=0' && rm -Rf Bleachbit          	 
			       ;;
			  23)
			        #Install Gkrellm
			        echo "================ Установка Gkrellm ================"
			        sudo apt install -y gkrellm 
				wget https://www.dropbox.com/sh/wafutq4xlczbza0/AAAy1l-wqCiLabOmhD82uCN8a?dl=0 && \
				unzip -q 'AAAy1l-wqCiLabOmhD82uCN8a?dl=0'
                                chmod +x Gkrellm/gkrellm.desktop
                                mv Gkrellm/gkrellm.desktop ~/.config/autostart/
				tar xvf Gkrellm/Bgkrellm2.tar.gz
				cp Gkrellm/GkrellmUninstall.sh ~/'Рабочий стол'/Uninstall 
                                rm -rf 'AAAy1l-wqCiLabOmhD82uCN8a?dl=0' && rm -Rf Gkrellm                                                                
			       ;;
			  24)
			        #Install SpeedtestWeb
		                echo "================ Установка SpeedtestWeb ================"
                                wget https://www.dropbox.com/sh/fj0l3tn6crbsfyg/AABqfHbT3o40SvyfD9yprFD-a?dl=0 && \
				unzip -q 'AABqfHbT3o40SvyfD9yprFD-a?dl=0'
                                chmod +x GTSSS/SpeedtestF.desktop
				cp GTSSS/SpeedtestF.desktop ~/.local/share/applications/
				sudo cp GTSSS/Speedtest.png /usr/share/icons/
				cp GTSSS/SpeedtestUninstall.sh ~/'Рабочий стол'/Uninstall 
                                rm -rf 'AABqfHbT3o40SvyfD9yprFD-a?dl=0' && rm -Rf GTSSS                                           
			       ;;
			  25)
			        #Install GoogleTranslateWeb
			        echo "================ Установка GoogleTranslateWeb ================"
                                wget https://www.dropbox.com/sh/fj0l3tn6crbsfyg/AABqfHbT3o40SvyfD9yprFD-a?dl=0 && \
				unzip -q 'AABqfHbT3o40SvyfD9yprFD-a?dl=0'
                                chmod +x GTSSS/GoogleTranslateF.desktop
				cp GTSSS/GoogleTranslateF.desktop ~/.local/share/applications/
				sudo cp GTSSS/GoogleTranslate.png /usr/share/icons/ 
				cp GTSSS/GoogleTranslateUninstall.sh ~/'Рабочий стол'/Uninstall            
                                rm -rf 'AABqfHbT3o40SvyfD9yprFD-a?dl=0' && rm -Rf GTSSS                                                             
			       ;;
			  26)
			        #Install SkypeWeb
		                echo "================ Установка SkypeWeb ================"
				wget https://www.dropbox.com/sh/fj0l3tn6crbsfyg/AABqfHbT3o40SvyfD9yprFD-a?dl=0 && \
				unzip -q 'AABqfHbT3o40SvyfD9yprFD-a?dl=0'
                                chmod +x GTSSS/SkypeF.desktop
				cp GTSSS/SkypeF.desktop ~/.local/share/applications/
				sudo cp GTSSS/Skype.png /usr/share/icons/
				cp GTSSS/SkypeUninstall.sh ~/'Рабочий стол'/Uninstall 
				rm -rf 'AABqfHbT3o40SvyfD9yprFD-a?dl=0' && rm -Rf GTSSS
			       ;;
		          27)
			        #Install SMS-Messages
				echo "================  Установка SMS-Messages ================"
                                wget https://www.dropbox.com/sh/fj0l3tn6crbsfyg/AABqfHbT3o40SvyfD9yprFD-a?dl=0 && \
				unzip -q 'AABqfHbT3o40SvyfD9yprFD-a?dl=0'
                                chmod +x GTSSS/SMSMessagesF.desktop
				cp GTSSS/SMSMessagesF.desktop ~/.local/share/applications/
				sudo cp GTSSS/SMSMessages.png /usr/share/icons/
				cp GTSSS/SMS-MessagesUninstall.sh ~/'Рабочий стол'/Uninstall 
				rm -rf 'AABqfHbT3o40SvyfD9yprFD-a?dl=0' && rm -Rf GTSSS
			       ;;
			  28)
			        #Install TelegramTsetup
		                echo "================ Установка TelegramTsetup ================"
				wget https://www.dropbox.com/sh/s3ybf17voaga97i/AAD1q9E-oOOZ39h7o_RpPZ_ca?dl=0 && \
				unzip -q 'AAD1q9E-oOOZ39h7o_RpPZ_ca?dl=0'
				tar xvf TelegraM/tsetup*.tar.xz
				sudo mv Telegram /opt/Telegram
				chmod +x TelegraM/*.desktop
                                cp TelegraM/org.telegram.desktop.*.desktop ~/.local/share/applications/
				sudo cp TelegraM/Telegram.png /usr/share/icons/
				cp TelegraM/Updater.desktop ~/.local/share/applications/
				sudo cp TelegraM/Updater.png /usr/share/icons/
				cp TelegraM/'Linux-это просто.png' ~/'Рабочий стол'/
				cp TelegraM/'Linux-это просто.sh' ~/'Рабочий стол'/
				cp TelegraM/TelegramUninstall.sh ~/'Рабочий стол'/Uninstall 
				rm -rf 'AAD1q9E-oOOZ39h7o_RpPZ_ca?dl=0' && rm -Rf TelegraM
		               ;;
			  29)
			        #Install SignalApt
				echo "================  Установка SignalApt ================"				
                                wget -O- https://updates.signal.org/desktop/apt/keys.asc | gpg --dearmor > signal-desktop-keyring.gpg
                                cat signal-desktop-keyring.gpg | sudo tee /usr/share/keyrings/signal-desktop-keyring.gpg > /dev/null
                                echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/signal-desktop-keyring.gpg] https://updates.signal.org/desktop/apt xenial main' |\
                                sudo tee /etc/apt/sources.list.d/signal-xenial.list
				rm -rf signal-desktop-keyring.gpg
                                sudo apt update && sudo apt install -y signal-desktop
				wget https://www.dropbox.com/sh/nszgf7iaeqluw33/AABeHiSD0fTZWifAOGWnWx01a?dl=0 && \
				unzip -q 'AABeHiSD0fTZWifAOGWnWx01a?dl=0'
				chmod +x Signal/signal-desktop.desktop
                                cp Signal/signal-desktop.desktop ~/.local/share/applications/
                                sudo cp Signal/Signal.png /usr/share/icons/	
				cp Signal/SignalUninstall.sh ~/'Рабочий стол'/Uninstall 			
                                rm -rf 'AABeHiSD0fTZWifAOGWnWx01a?dl=0' rm -Rf Signal
			       ;;
			  30)
			        #Install WireWeb
		                echo "================ Установка WireWeb ================"
                                wget https://www.dropbox.com/sh/cchy00tljr431il/AABPjdODeAHavyraKdqF91-ba?dl=0 && \
				unzip -q 'AABPjdODeAHavyraKdqF91-ba?dl=0'
                                chmod +x Wire/WireF.desktop
				cp Wire/WireF.desktop ~/.local/share/applications/
				sudo cp Wire/Wire.png /usr/share/icons/
				cp Wire/WireUninstall.sh ~/'Рабочий стол'/Uninstall 
				rm -rf 'AABPjdODeAHavyraKdqF91-ba?dl=0' && rm -Rf Wire                    
			       ;;
                          31)
			        #Install WhatsAppDeb
		                echo "================ Установка WhatsAppDeb ================"
                                wget https://www.dropbox.com/sh/itnkex987dv63jh/AADCkyWHaZq9cfhBb0QBghlqa?dl=0 && \
				unzip -q 'AADCkyWHaZq9cfhBb0QBghlqa?dl=0'				
                                sudo dpkg -i Whatsapp/'whatsapp-for-linux_1.6.3_amd64.deb'
				sudo apt install -fy
				sudo cp Whatsapp/Whatsapp.png /usr/share/icons/ 
				chmod +x Whatsapp/com.github.eneshecan.WhatsAppForLinux.desktop
				cp Whatsapp/com.github.eneshecan.WhatsAppForLinux.desktop ~/.local/share/applications/ 
				cp Whatsapp/WhatsAppUninstall.sh ~/'Рабочий стол'/Uninstall                                                       
				rm -rf 'AADCkyWHaZq9cfhBb0QBghlqa?dl=0' && rm -Rf Whatsapp                            
			       ;;
			  32)
		                #Install ViberAppImage
                                echo "================ Установка ViberAppImage ================"
                                wget https://www.dropbox.com/sh/5c88ucnin6uae0h/AABQCi4foO5jVO-9AXS-unXea?dl=0 && \
				unzip -q 'AABQCi4foO5jVO-9AXS-unXea?dl=0'
                                chmod +x Viber/ViberApp.desktop
				cp Viber/ViberApp.desktop ~/.local/share/applications/
				sudo cp Viber/Viber.png /usr/share/icons/
				chmod +x ./Viber/*.AppImage
                                sudo mv Viber/viber.AppImage /opt/
				cp Viber/ViberUninstall.sh ~/'Рабочий стол'/Uninstall 
				rm -rf 'AABQCi4foO5jVO-9AXS-unXea?dl=0' && rm -Rf Viber
			       ;;
			  33)
			        #Install Cherrytree
			        echo "================ Установка Cherrytree ================"
				sudo apt-get install -y cherrytree                                                            				
				wget https://www.dropbox.com/sh/ev9n5ttb3cf99u7/AAAWZl4luvIIk3BJEfX-eRTWa?dl=0 && \
				unzip -q 'AAAWZl4luvIIk3BJEfX-eRTWa?dl=0'                   				                                
                                chmod -R 777 Cherrytree                         
                                sudo cp Cherrytree/Cherrytree.svg /usr/share/icons/  
                                cp Cherrytree/cherrytree.desktop ~/.local/share/applications/
				cp Cherrytree/ПаролиLiveссылки.ctb ~/'Рабочий стол'/
                                cp -r Cherrytree/cherrytree ~/.config/  
				cp Cherrytree/CherrytreeUninstall.sh ~/'Рабочий стол'/Uninstall 
                                rm -rf 'AAAWZl4luvIIk3BJEfX-eRTWa?dl=0' && rm -Rf Cherrytree            		              
			       ;;
			  34)
			        #Install Gotop
				echo "================ Установка Gotop ================"
                                wget https://www.dropbox.com/sh/d5ugl4dc43jcmxs/AADSkJBfQdCiJP4Io_8uNmcNa?dl=0 && \
				unzip -q 'AADSkJBfQdCiJP4Io_8uNmcNa?dl=0'
                                chmod +x Gotop/gotop				
				sudo cp Gotop/gotop /opt/ 
				sudo cp Gotop/Gotop.png /usr/share/icons/
                                chmod +x Gotop/Gotop.desktop
                                cp Gotop/Gotop.desktop ~/.local/share/applications/   
				cp Gotop/GotopUninstall.sh ~/'Рабочий стол'/Uninstall                                                               				  
                                rm -rf 'AADSkJBfQdCiJP4Io_8uNmcNa?dl=0' && rm -Rf Gotop	                                            
			       ;;
			  35)
			        #Install AnydeskDeb
				echo "================ Установка AnydeskDeb ================"
                                wget https://www.dropbox.com/sh/1vvd99olkmtraih/AACM_x29BDmbtW7CtaglwFwfa?dl=0 && \
				unzip -q 'AACM_x29BDmbtW7CtaglwFwfa?dl=0'
                                sudo dpkg -i AnyDesk/anydesk*.deb
                                sudo apt install -fy		
				cp AnyDesk/AnydeskUninstall.sh ~/'Рабочий стол'/Uninstall                               
                                rm -rf 'AACM_x29BDmbtW7CtaglwFwfa?dl=0' && rm -Rf AnyDesk	  
			       ;;
			  36)
			        #Install Galkulator
			        echo "================ Установка Galkulator ================"
				sudo apt install -y galculator
				wget https://www.dropbox.com/sh/8h6xnu2fv70cpfa/AADyQHGhg1RZlxp4J7FBBGIKa?dl=0 && \
				unzip -q 'AADyQHGhg1RZlxp4J7FBBGIKa?dl=0'
                                chmod +x Calkulator/*.desktop
				cp Calkulator/galculator.desktop ~/.local/share/applications/
				sudo cp Calkulator/Calkulator.svg /usr/share/icons/
				cp Calkulator/CalkulatorUninstall.sh ~/'Рабочий стол'/Uninstall
				rm -rf  'AADyQHGhg1RZlxp4J7FBBGIKa?dl=0' && rm -Rf Calkulator
		 	       ;;			
			  37)
			        #Install BauhAppImage
			        echo "================ Установка BauhAppImage ================"	
                                sudo apt-get install -y python3 python3-pip python3-yaml python3-dateutil \
				python3-pyqt5 python3-packaging python3-requests                                
				wget https://www.dropbox.com/sh/3o0is46szlbc7jp/AABXRmQ_h8fNx6uAFbpj84dua?dl=0 && \
				unzip -q 'AABXRmQ_h8fNx6uAFbpj84dua?dl=0'                                                           
                                chmod +x Bauh/BauhApp.desktop
                                cp Bauh/BauhApp.desktop ~/.local/share/applications/
                                sudo cp Bauh/Bauh.svg /usr/share/icons/ 
                                chmod +x ./Bauh/*.AppImage
				sudo mv Bauh/bauh*.AppImage /opt/bauh.AppImage  
				cp Bauh/BauhUninstall.sh ~/'Рабочий стол'/Uninstall                         
                                rm -rf 'AABXRmQ_h8fNx6uAFbpj84dua?dl=0' && rm -Rf Bauh
			       ;;
			  38)
			        #Install LiveUSBMakerQtAppImageDDSh   
			        echo "================ Установка LiveUSBMakerQtAppImageDDSh ================"
				sudo apt-get install -y extlinux syslinux-common
			        wget https://www.dropbox.com/sh/om8mkd01k6rouua/AADXp3fQj2SeYjiGURt9MpQIa?dl=0 && \
				unzip -q 'AADXp3fQj2SeYjiGURt9MpQIa?dl=0'			 				                                                                 
				chmod +x LumQtApp/QtAppImageDDShApl                                                                                                                                                                      
                                cp -r LumQtApp/QtAppImageDDShApl ~/.local/share/applications/
				chmod -R 777 LumQtApp/QtAppImageDDSh
				sudo cp -r LumQtApp/QtAppImageDDSh /usr/share/icons/
				chmod +x ./LumQtApp/*.AppImage 
				sudo mv LumQtApp/live-usb-maker-qt*.AppImage /usr/local/bin/LiveUSBMakerQt.AppImage
				sudo cp LumQtApp/LiveUSBMaker.AppImage /usr/local/bin/
				sudo cp LumQtApp/DDLiveUsb.AppImage /usr/local/bin/
				tar xvf LumQtApp/DDLiveUSBMakerSh.tar.gz
				sudo mv DDLiveUSBMakerSh /usr/local/bin/DDLiveUSBMakerSh
				sudo chown -R $USER:$USER /usr/local/bin/DDLiveUSBMakerSh								
				cp LumQtApp/DDLiveUsbAppImage.png ~/'Рабочий стол'/   
				cp LumQtApp/LiveUSBMakerAppImage.png ~/'Рабочий стол'/	
				cp LumQtApp/LiveUSBMakerQtAppImage.png ~/'Рабочий стол'/	
				cp LumQtApp/LiveUSBMakerQtAppImageDDShUninstall.sh ~/'Рабочий стол'/Uninstall
				rm -rf 'AADXp3fQj2SeYjiGURt9MpQIa?dl=0' && rm -Rf LumQtApp			 			 
			       ;;
			  39)
			        #Install Mintstick
		                echo "================ Установка Mintstick ======================"
				wget https://www.dropbox.com/sh/6bz3gmomboihu7j/AACErhxW5xoPNi3vQEX3hq0ua?dl=0 && \
				unzip -q 'AACErhxW5xoPNi3vQEX3hq0ua?dl=0'                               
                                sudo dpkg -i Mintstick/*.deb
		                sudo apt install -f -y                                        
                                chmod +x Mintstick/*.desktop
                                cp Mintstick/mintstick.desktop ~/.local/share/applications/
				cp Mintstick/mintstick-format.desktop ~/.local/share/applications/  
				chmod -R 777 Mintstick/mintstick
                                sudo cp -R Mintstick/mintstick /usr/share/icons/                                                            				  
				chmod +x Mintstick/sysctl.conf
                                sudo cp Mintstick/sysctl.conf /etc/sysctl.conf 
				cp Mintstick/MintstickUninstall.sh ~/'Рабочий стол'/Uninstall  
				#Ссылка на удержание пакета #https://qa.yodo.im/t/kak-predotvratit-obnovlenie-opredelennogo-paketa/3350/1
				#echo "mintstick hold" | sudo dpkg --set-selections
                                rm -rf 'AACErhxW5xoPNi3vQEX3hq0ua?dl=0' && rm -Rf Mintstick 
			       ;;
			  40)
			        #Install VentoyWebTar
			        echo "================ Установка VentoyWebTar ================"  
				wget https://www.dropbox.com/sh/8xszka2m3dtyugy/AAD7pz3NICDHsOMkungofVeCa?dl=0 && \
				unzip -q 'AAD7pz3NICDHsOMkungofVeCa?dl=0'
				tar xvf VentoyWeb/ventoy*.tar.gz 
				sudo mv ventoy* /opt/ventoy
				sudo chown -R $USER:$USER /opt/ventoy			                                                                             
                                chmod +x VentoyWeb/VentoyWeb.desktop  
                                cp VentoyWeb/VentoyWeb.desktop ~/.local/share/applications/
                                sudo cp VentoyWeb/Ventoy.png /usr/share/icons/  
				cp VentoyWeb/VentoyWebUninstall.sh ~/'Рабочий стол'/Uninstall 
                                rm -rf 'AAD7pz3NICDHsOMkungofVeCa?dl=0' && rm -Rf VentoyWeb
			       ;;
			  41)
			        #Install MultiBootUSB
				echo "================ Установка MultiBootUSB ================"
                                wget https://www.dropbox.com/sh/tfngu17tt3dnj4p/AADNU6lEkYmi35rFMJPsNVeRa?dl=0 && \
				unzip -q 'AADNU6lEkYmi35rFMJPsNVeRa?dl=0'
                                sudo dpkg -i MultiBootUSB/python3-multibootusb*.deb
				sudo apt install -fy                                 					 
                                chmod +x MultiBootUSB/multibootusb.desktop                                                              
                                cp MultiBootUSB/multibootusb.desktop ~/.local/share/applications/
                                sudo cp MultiBootUSB/MultiBootUsb.svg /usr/share/icons/ 
				cp MultiBootUSB/MultiBootUSBUninstall.sh ~/'Рабочий стол'/Uninstall 
                                rm -rf 'AADNU6lEkYmi35rFMJPsNVeRa?dl=0' && rm -Rf MultiBootUSB
			       ;;
			  42)
                               #Install Unetbootin 
			        echo "================ Установка Unetbootin ================"
                                sudo apt install -y xclip mtools extlinux
                                wget https://www.dropbox.com/sh/71p463jvkqkzmpz/AACuBEC58iO1vLxoUz5FuZKOa?dl=0 && \
				unzip -q 'AACuBEC58iO1vLxoUz5FuZKOa?dl=0'			                                
                                chmod +x Unetbootin/unetbootin*.bin
                                sudo mv Unetbootin/unetbootin*.bin /opt/unetbootin.bin                                				 
				chmod +x Unetbootin/Unetbootin.desktop                                                
				cp Unetbootin/Unetbootin.desktop ~/.local/share/applications/
                                sudo cp Unetbootin/Unetbootin.png /usr/share/icons/   
				cp Unetbootin/UnetbootinUninstall.sh ~/'Рабочий стол'/Uninstall 
                                rm -rf 'AACuBEC58iO1vLxoUz5FuZKOa?dl=0' && rm -Rf Unetbootin 
			       ;;
			  43)
				#Install  EtcherAppImage 
			        echo "================ Установка EtcherAppImage ================"
			        wget https://www.dropbox.com/sh/dajkpg6rwof3ru2/AACn4uvLuERyraJVZNio42nAa?dl=0 && \
				unzip -q 'AACn4uvLuERyraJVZNio42nAa?dl=0'                                                                           
                                chmod +x Etcher/EtcherApp.desktop
                                cp Etcher/EtcherApp.desktop ~/.local/share/applications/
                                sudo cp Etcher/Etcher.png /usr/share/icons/ 
                                chmod +x ./Etcher/*.AppImage
				sudo mv Etcher/balenaEtcher*.AppImage /opt/Etcher.AppImage
				cp Etcher/EtcherUninstall.sh ~/'Рабочий стол'/Uninstall 
                                rm -rf 'AACn4uvLuERyraJVZNio42nAa?dl=0' && rm -Rf Etcher  
				;;
			   44)
				#Install RosaImageWriter
                                echo "================ Установка RosaImageWriter ================"
				wget https://www.dropbox.com/sh/dr3j9cee5db2wk6/AABV9sSW_anWxoZMqws9iLqoa?dl=0 && \
				unzip -q 'AABV9sSW_anWxoZMqws9iLqoa?dl=0'                               
                                chmod +x ./RosaIW/RosaImageWriter
                                sudo cp RosaIW/RosaImageWriter /opt/                                
                                chmod +x RosaIW/RosaImageWriter.desktop
                                cp RosaIW/RosaImageWriter.desktop ~/.local/share/applications/
                                sudo cp RosaIW/RosaImageWriter.png /usr/share/icons/   
				cp RosaIW/RosaImageWriterUninstall.sh ~/'Рабочий стол'/Uninstall 
                                rm -rf 'AABV9sSW_anWxoZMqws9iLqoa?dl=0' && rm -Rf RosaIW 
				;;
			   45)
				#Install ddCopy
			        echo "================ Установка ddCopy ================"                            
			        wget https://www.dropbox.com/sh/t4b3rja97a04tsc/AAAEyWsv5YAGcujFRnX6vGjYa?dl=0 && \
				unzip -q 'AAAEyWsv5YAGcujFRnX6vGjYa?dl=0'
			        sudo dpkg -i ddCopy/ddcopy*.deb
			        sudo apt install -fy  
				cp ddCopy/ddCopyDebUninstall.sh ~/'Рабочий стол'/Uninstall 
                                rm -rf 'AAAEyWsv5YAGcujFRnX6vGjYa?dl=0' && rm -Rf ddCopy                             
				;;
			   46)
				#Install Дополнительные драйвера Wi-Fi realtek
                                echo "================ Установка Wi-Fi realtek ========================="
				sudo apt install -y firmware-linux firmware-linux-nonfree firmware-realtek                                       			 
				;;
			   47)
			        #Install Дополнительные драйвера Wi-Fi Intel
				echo "================ Установка Wi-Fi intel =========================="
                                sudo apt install -y firmware-linux firmware-linux-nonfree firmware-iwlwifi     		         
			       ;;
			  48)
			        #Install Дополнительные драйвера Wi-Fi Marvell и NXP (Libertas)
			        echo "================ Установка Wi-Fi Marvell и NXP (Libertas) ============="	
                                sudo apt install -y firmware-linux firmware-linux-nonfree firmware-libertas
			       ;;			
			  49)
			        #Install Дополнительные драйвера Wi-Fi broadcom
			        echo "================ Установка Дополнительные драйвера Wi-Fi broadcom ================" 
                                sudo apt install -y firmware-linux firmware-linux-nonfree broadcom-sta-dkms                             
			       ;;
		          50)
                                #Install Дополнительные драйвера Wi-Fi atheros
				echo "================ Установка Wi-Fi atheros ================"
                                sudo apt install -y firmware-linux firmware-linux-nonfree firmware-atheros 
			       ;;
			  51)
			        #Install Любая Ваша программа
			        echo "================ Установка Любая Ваша программа ================"                        			 
			       ;;
			  52)
			        #Install Любая Ваша программа
			        echo "================ Установка Любая Ваша программа ================"                         		       				                                                                   				 
			       ;;
			  53)
			        #Install Любая Ваша программа
			        echo "================ Установка Любая Ваша программа ================"	                                                            
			       ;;
			  54)
			        #Install Любая Ваша программа
			        echo "================ Установка Любая Ваша программа ================"  		       
			       ;;
			  55)
			        #Install Любая Ваша программа
			        echo "================ Установка Любая Ваша программа ================"                    
			       ;;
			  56)
			        #Install Любая Ваша программа
			        echo "================ Установка Любая Ваша программа ================"
			       ;;
			  57)
			        #Install Любая Ваша программа
			        echo "================ Установка Любая Ваша программа ================"		       
			       ;;
			  58)
			        #Install Любая Ваша программа
			        echo "================ Установка Любая Ваша программа ================"
			       ;;
			  59)
			        #Install Любая Ваша программа
			        echo "================ Установка Любая Ваша программа ================"                                
			       ;;
			  60)
			        #Install Любая Ваша программа
			        echo "================ Установка Любая Ваша программа ================"
			       ;;
			  61)
			        #Install Любая Ваша программа
			        echo "================ Установка Любая Ваша программа ================"         	                                
			       ;;
			  62)
			        #Install Любая Ваша программа
			        echo "================ Установка Любая Ваша программа ================" 
			       ;;
			  63)
			        #Install Любая Ваша программа
			        echo "================ Установка Любая Ваша программа ================"                               
			       ;;
			  64)
			        #Install Любая Ваша программа
			        echo "================ Установка Любая Ваша программа ================" 				 
			       ;;
			  65)
			        #Install Любая Ваша программа
			        echo "================ Установка Любая Ваша программа ================"          
			       ;;
		          66)
			        #Install Любая Ваша программа
			        echo "================ Установка Любая Ваша программа ================"                           
			       ;;
			  67)
			        #Install Любая Ваша программа
			        echo "================ Установка Любая Ваша программа ================"                    
			       ;;
			  68)
                                #Install Любая Ваша программа
			        echo "================ Установка Любая Ваша программа ================"			
			       ;;
			  69)
			        #Install Любая Ваша программа
			        echo "================ Установка Любая Ваша программа ================"                            
			       ;;
			  70)
			        #Install UnattendedUpgrades
		                echo "================ Установка UnattendedUpgrades ================"
				wget https://www.dropbox.com/sh/n78t9ogufcpbiw0/AAB7HYdU94o_7BKIP1KUPGjWa?dl=0 && \
				unzip -q 'AAB7HYdU94o_7BKIP1KUPGjWa?dl=0'
                                sudo apt install -y unattended-upgrades
				cp UnattendedUpgrades/UnattendedUpgradesInfo.sh ~/'Рабочий стол'/
				rm -rf 'AAB7HYdU94o_7BKIP1KUPGjWa?dl=0' && rm -Rf UnattendedUpgrades
			       ;;
		          71)
			        #Install AppImageUpdate
				echo "================ Установка AppImageUpdate ================"      
				wget https://www.dropbox.com/sh/0jjoic2u2qiu2gl/AABso4fr297VkkQEuGcFuZz3a?dl=0 && \
				unzip -q 'AABso4fr297VkkQEuGcFuZz3a?dl=0'			
				chmod +x AppImageUpdate/AppImageUpdate.desktop
				cp AppImageUpdate/AppImageUpdate.desktop  ~/.local/share/applications/
				sudo cp AppImageUpdate/AppImageUpdate.png /usr/share/icons/
				chmod +x ./AppImageUpdate/*.AppImage
				sudo mv AppImageUpdate/AppImageUpdate*.AppImage  /opt/AppImageUpdate.AppImage	
				cp AppImageUpdate/AppImageUpdateUninstall.sh ~/'Рабочий стол'/Uninstall		
				rm -rf 'AABso4fr297VkkQEuGcFuZz3a?dl=0' && rm -Rf AppImageUpdate 	                         	 				                                  
			       ;;
                          72)
			        #Install MOCP-MusicOnConsolePlayer
				echo "================ Установка MOCP-MusicOnConsolePlayer ================"
				sudo apt install -y moc moc-ffmpeg-plugin
				wget https://www.dropbox.com/sh/980q1ps3l86oi2a/AAAfyy_SABXnaMRDSNrrJ7uja?dl=0 && \
				unzip -q 'AAAfyy_SABXnaMRDSNrrJ7uja?dl=0'
				chmod +x MOCP/Moc.desktop
				cp MOCP/Moc.desktop ~/.local/share/applications/
				sudo cp MOCP/Moc.png /usr/share/icons/
				cp MOCP/Accept-AmamosLaVida.mp3 ~/'Рабочий стол'/
				cp MOCP/MOCP-MusicOnConsolePlayer.sh ~/'Рабочий стол'/
				cp MOCP/MOCPUninstall.sh ~/'Рабочий стол'/Uninstall 
				rm -rf 'AAAfyy_SABXnaMRDSNrrJ7uja?dl=0' && rm -Rf MOCP
                               ;;
                          73)
			        #Install Transmission
				echo "================ Установка Transmission ================"
				sudo apt install -y transmission transmission-cli transmission-gtk transmission-qt
				wget https://www.dropbox.com/sh/y6xvctjm5r9a6o3/AABZEq7tq3rNX9O0RLaIppIza?dl=0 && \
				unzip -q 'AABZEq7tq3rNX9O0RLaIppIza?dl=0'
				cp transmission/TransmissionUninstall.sh ~/'Рабочий стол'/Uninstall 
				rm -rf 'AABZEq7tq3rNX9O0RLaIppIza?dl=0' && rm -Rf transmission 
                               ;;
                          74)
			        #Install QBittorrent
				echo "================ Установка QBittorrent ================"
				sudo apt install -y qbittorrent
				wget https://www.dropbox.com/sh/83ftoqo026abqgg/AABm3SnbVcFxwL3kMhpIcB8da?dl=0 && \
				unzip -q 'AABm3SnbVcFxwL3kMhpIcB8da?dl=0'
				chmod +x QBittorrent/org.qbittorrent.qBittorrent.desktop
				cp QBittorrent/org.qbittorrent.qBittorrent.desktop ~/.local/share/applications/
				sudo cp QBittorrent/QBittorrent.png /usr/share/icons/
				cp QBittorrent/QBittorrentUninstall.sh ~/'Рабочий стол'/Uninstall       
				rm -rf 'AABm3SnbVcFxwL3kMhpIcB8da?dl=0' && rm -Rf QBittorrent
                               ;;
                          75)
			       #Install Persepolis 
				echo "================ Установка Persepolis ================"	
                                sudo apt install -y aria2 persepolis
				wget https://www.dropbox.com/sh/t5zggq9dzns19ap/AAANV8d7XXMxzympXfUWMo31a?dl=0 && \
				unzip -q 'AAANV8d7XXMxzympXfUWMo31a?dl=0'
                                cp Persepolis/Persepolis.png ~/'Рабочий стол'/
                                cp Persepolis/PersepolisUninstall.sh ~/'Рабочий стол'/Uninstall 
                                rm -rf 'AAANV8d7XXMxzympXfUWMo31a?dl=0' && rm -Rf Persepolis     
                               ;;
                          76)
			        #Install ClipGrabAppImage
			        echo "================ Установка ClipGrabAppImage ================"				 
                                wget https://www.dropbox.com/sh/jxap3clejvvii2e/AAC61QrPWcy7X9FbSVMnrVeGa?dl=0 && \
				unzip -q 'AAC61QrPWcy7X9FbSVMnrVeGa?dl=0'		         		       
                                chmod +x ClipGrabApp/ClipGrabApp.desktop
                                cp ClipGrabApp/ClipGrabApp.desktop ~/.local/share/applications/
                                sudo cp ClipGrabApp/ClipGrabApp.png /usr/share/icons/                                 
				chmod +x ./ClipGrabApp/*.AppImage
				sudo mv ClipGrabApp/ClipGrab*.AppImage /opt/ClipGrab.AppImage
				cp ClipGrabApp/ClipGrabUninstall.sh ~/'Рабочий стол'/Uninstall 
				rm -rf 'AAC61QrPWcy7X9FbSVMnrVeGa?dl=0' && rm -Rf ClipGrabApp		       		 			       
                               ;;
                          77)
			        #Install XDM
				echo "================ Установка XDM ================"
				wget https://www.dropbox.com/sh/9xwhut480sofbjd/AAAdfIqtIhWjOdOKPMG76RL0a?dl=0 && \
				unzip -q 'AAAdfIqtIhWjOdOKPMG76RL0a?dl=0'                             
				tar xvf XDM/xdm-setup*.tar.xz
				sudo ./install.sh
				cp XDM/XDMUninstall.sh ~/'Рабочий стол'/Uninstall 
				rm -rf 'AAAdfIqtIhWjOdOKPMG76RL0a?dl=0' && rm -Rf XDM
				rm -rf install.sh && rm -rf readme.txt && rm -R javasharedresources
                               ;;
                          78)
			       #Install YoutubeDL+Gui 
			       echo "================ Установка YoutubeDL+Gui ================"
			       sudo apt install -y youtubedl-gui aria2
			       wget https://www.dropbox.com/sh/a145hfqvpzn3axh/AAD_snHKMZjq7hwtqEYecKf8a?dl=0 && \
			       unzip -q 'AAD_snHKMZjq7hwtqEYecKf8a?dl=0'
			       cp YoutubeDl+Gui/YoutubeDL+GuiUninstall.sh ~/'Рабочий стол'/Uninstall
                               rm -rf 'AAD_snHKMZjq7hwtqEYecKf8a?dl=0' && rm -Rf YoutubeDl+Gui                                                                                              	
                               ;;
                          79)
			        #Install Yt-Dlp
			        echo "================ Установка Yt-Dlp ================"
                                sudo apt install -y yt-dlp ffmpeg    
				wget https://www.dropbox.com/sh/vxwefpe3kz91rxe/AAAdvdmnkLQc4JLI2hy6W1vQa?dl=0 && \
				unzip -q 'AAAdvdmnkLQc4JLI2hy6W1vQa?dl=0'
				mkdir ~/.local/bin
				chmod +x yt-dlp/yt-dlp
				cp yt-dlp/yt-dlp ~/.local/bin/
				echo 'export PATH=~/.local/bin:$PATH' >> ~/.bashrc
				cp yt-dlp/Yt-DlpOption.sh ~/'Рабочий стол'/
				rm -rf 'AAAdvdmnkLQc4JLI2hy6W1vQa?dl=0' && rm -Rf yt-dlp 
                               ;;
                          80)
			        #Install JDownloader2 
				echo "================ Установка JDownloader2 ================"
                                sudo apt install -y default-jdk
                                wget https://www.dropbox.com/sh/wv25tzc9utsmfzd/AAAcfrdv3iS3Kg_O2rGqAE_Ha?dl=0 && \
				unzip -q 'AAAcfrdv3iS3Kg_O2rGqAE_Ha?dl=0'	
		                chmod +x Descargas/JD*
				sudo sh Descargas/JDownloader2Setup_unix_nojre.sh
				chmod +x Descargas/*desktop
				cp Descargas/UninstallJDownloader.desktop ~/.local/share/applications/ 
				sudo cp Descargas/JDownloader2.png /usr/share/icons/
				cp Descargas/JDownloaderUninstall.sh ~/'Рабочий стол'/Uninstall 
                                rm -rf 'AAAcfrdv3iS3Kg_O2rGqAE_Ha?dl=0' && rm -Rf Descargas
                               ;;
                          81)
			        #Install MediaDownloader
				echo "================  Установка MediaDownloader ================"
                                sudo apt install -y media-downloader yt-dlp aria2 ffmpeg 
				wget https://www.dropbox.com/sh/2vzwr29ywi8dctq/AABBYlekZpn_qGxB1OR6rVbea?dl=0 && \
				unzip -q 'AABBYlekZpn_qGxB1OR6rVbea?dl=0'
                                tar xvf MediaDownloader/media-downloader.tar.gz 
                                mv media-downloader ~/.config/media-downloader
				cp MediaDownloader/MediaDownloaderUninstall.sh ~/'Рабочий стол'/Uninstall 
                                rm -rf 'AABBYlekZpn_qGxB1OR6rVbea?dl=0' && rm -Rf MediaDownloader                            
                               ;;
                          82)
			        #Install MotrixApp
			        echo "================ MotrixApp ================"
				wget https://www.dropbox.com/sh/0jgherokzw8un8t/AABGT5Kp9L9N2di9UFKsxdQza?dl=0 && \
				unzip -q 'AABGT5Kp9L9N2di9UFKsxdQza?dl=0'				
				chmod +x MotrixApp/MotrixApp.desktop
                                cp MotrixApp/MotrixApp.desktop ~/.local/share/applications/
                                sudo cp MotrixApp/MotrixApp.png /usr/share/icons/
				chmod +x ./MotrixApp/*.AppImage				                                          
				sudo mv MotrixApp/Motrix*.AppImage /opt/Motrix.AppImage   
				tar xvf MotrixApp/Motrix.tar.gz  
				mv Motrix ~/.config/Motrix       
				cp MotrixApp/MotrixUninstall.sh ~/'Рабочий стол'/Uninstall                                             
				rm -rf 'AABGT5Kp9L9N2di9UFKsxdQza?dl=0' && rm -Rf MotrixApp
                               ;;
                          83)
			        #Install Любая Ваша программа
			        echo "================ Установка Любая Ваша программа ================"                   		 
                               ;;
                          84)
			        #Install Любая Ваша программа
			        echo "================ Установка Любая Ваша программа ================" 
                               ;;
                          85)
			        #Install Pluma
				echo "================ Установка Pluma ================"
                                sudo apt install -y pluma
				wget https://www.dropbox.com/sh/h95quyaq50u9337/AABLLDhjc_XNOH8ZzXF_vwQTa?dl=0 && \
				unzip -q 'AABLLDhjc_XNOH8ZzXF_vwQTa?dl=0'
				tar xvf  Pluma/pluma.tar.gz
				mv pluma ~/.config/pluma	
				cp Pluma/PlumaUninstall.sh ~/'Рабочий стол'/Uninstall 		
				rm -rf 'AABLLDhjc_XNOH8ZzXF_vwQTa?dl=0' && rm -Rf Pluma
                               ;;
                          86)
			        #Install Любая Ваша программа
			        echo "================ Установка Любая Ваша программа ================"  
                               ;;
                          87)
			        #Install Featherpad
			        echo "================ Установка Featherpad ================"    
                                sudo apt install -y featherpad featherpad-l10n
				wget https://www.dropbox.com/sh/h95quyaq50u9337/AABLLDhjc_XNOH8ZzXF_vwQTa?dl=0 && \
				unzip -q 'AABLLDhjc_XNOH8ZzXF_vwQTa?dl=0'
				tar xvf  Pluma/featherpad.tar.gz
				mv featherpad ~/.config/featherpad			
				cp Pluma/FeatherPadUninstall.sh ~/'Рабочий стол'/Uninstall
				rm -rf 'AABLLDhjc_XNOH8ZzXF_vwQTa?dl=0' && rm -Rf Pluma                          
                               ;;
                          88)
			        #Install LeafpadSourceFile
		                echo "================ Установка LeafpadSourceFile ================"
                                wget https://www.dropbox.com/sh/ublhpdts1wlskau/AAD6ELxDOj4LVGtsFCg4Y0PDa?dl=0 && \
				unzip -q 'AAD6ELxDOj4LVGtsFCg4Y0PDa?dl=0'
				#'Установка LeafPad на Linux с использованием исходного файла'
				sudo apt-get update && sudo apt-get upgrade -y
				#'Установите необходимые для LeafPad зависимости с помощью следующей команды'
				sudo apt-get install build-essential checkinstall intltool libgtk2.0-dev -y
				tar xvf LeafpaD/LEAF.tar.gz
				cd Leafpad
				./configure && make && \
				sudo checkinstall -D --install=yes --fstrans=no --pakdir "$HOME/leafpad_build" \
				--pkgname leafpad --backup=no --deldoc=yes --deldesc=yes --delspec=yes \
				--default --pkgversion "0.8.9" && \
				make distclean && sudo ldconfig
				cd ~
				rm -Rf Leafpad && sudo rm -Rf leafpad_build
				cp LeafpaD/leafpad.desktop ~/.local/share/applications/
				sudo cp LeafpaD/leafpad.png /usr/share/icons/
                                tar xvf LeafpaD/leafpad.tar.gz
                                mv leafpad ~/.config/leafpad 				
				cp LeafpaD/LeafpadUninstall.sh ~/'Рабочий стол'/Uninstall
                                rm -rf 'AAD6ELxDOj4LVGtsFCg4Y0PDa?dl=0' && rm -Rf LeafpaD
                               ;;
                          89)
			        #Install Spedit
		                echo "================ Установка Spedit ================"
                                wget https://www.dropbox.com/sh/2g2wdsyyg3bc42l/AADXlO6rG2zswOpVZbe-FQgCa?dl=0 && /
				unzip -q 'AADXlO6rG2zswOpVZbe-FQgCa?dl=0'
				sudo dpkg -i L3afpad/spedit*.deb
		                sudo apt install -f -y
				chmod +x L3afpad/Spedit.desktop
                                cp L3afpad/Spedit.desktop ~/.local/share/applications/
				sudo mkdir /usr/local/share/pixmaps/
                                sudo cp L3afpad/l3afpad.png /usr/local/share/pixmaps/
                                tar xvf L3afpad/l3afpad.tar.gz
                                mv l3afpad ~/.config/l3afpad 
				cp L3afpad/SpeditUninstall.sh ~/'Рабочий стол'/Uninstall
                                rm -rf 'AADXlO6rG2zswOpVZbe-FQgCa?dl=0' && rm -Rf L3afpad                                                                  
                               ;;
                          90)
			        #Install MeditApt
				echo "================ Установка MeditApt ================"
				echo 'deb http://download.opensuse.org/repositories/home:/antonbatenev:/medit/Debian_11/ /' | \
				sudo tee /etc/apt/sources.list.d/home:antonbatenev:medit.list
				curl -fsSL https://download.opensuse.org/repositories/home:antonbatenev:medit/Debian_12/Release.key | \
				gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/home_antonbatenev_medit.gpg > /dev/null
				sudo apt update && sudo apt install -y medit
				wget https://www.dropbox.com/sh/w626a29qh6h9aq6/AACp42Vt8lRAMR72MDwxq63ta?dl=0 && \
				unzip -q 'AACp42Vt8lRAMR72MDwxq63ta?dl=0'								
				tar xvf Medit/medit.tar.gz
                                mv medit ~/.local/share/medit
				 cp Medit/MeditUninstall.sh ~/'Рабочий стол'/Uninstall 
                                rm -rf 'AACp42Vt8lRAMR72MDwxq63ta?dl=0' && rm -Rf Medit                                                   
                               ;;
                          91)
		                #Install Gedit
				echo "================ Установка Gedit ================"
				sudo apt install -y gedit
                                wget https://www.dropbox.com/sh/ye4pn6aattmdt0s/AAAM6euX_TeGh3yOP7hbXpsea?dl=0 && \
				unzip -q 'AAAM6euX_TeGh3yOP7hbXpsea?dl=0'
                                chmod +x Gedit/org.gnome.gedit.desktop
                                cp Gedit/org.gnome.gedit.desktop ~/.local/share/applications/
                                sudo cp Gedit/Gedit.svg /usr/share/icons/ 
				tar xvf Gedit/gedit.tar.gz
				mv gedit ~/.config/gedit
				cp Gedit/GeditUninstall.sh ~/'Рабочий стол'/Uninstall  
                                rm -rf 'AAAM6euX_TeGh3yOP7hbXpsea?dl=0' && rm -Rf Gedit                             		 
                               ;;
                          92)
			        #Install OpenOfficeApp
			        echo "================ Установка OpenOfficeApp ================"	
				wget https://www.dropbox.com/sh/inf0p3shz56dggm/AABiwJqm22j3PQg0qRf13pV7a?dl=0 && \
				unzip -q 'AABiwJqm22j3PQg0qRf13pV7a?dl=0'                                                                              
                                chmod +x OpenOfficeApp/OpenOfficeApp.desktop
                                cp OpenOfficeApp/OpenOfficeApp.desktop ~/.local/share/applications/ 
                                sudo cp OpenOfficeApp/OpenOffice.png /usr/share/icons/ 
				chmod +x ./OpenOfficeApp/*.AppImage 
				sudo mv OpenOfficeApp/OpenOffice*.AppImage /opt/OpenOffice.AppImage 
				cp OpenOfficeApp/OpenOfficeUninstall.sh ~/'Рабочий стол'/Uninstall                      
                                rm -rf 'AABiwJqm22j3PQg0qRf13pV7a?dl=0' && rm -Rf OpenOfficeApp                             
                               ;;
                          93)
			        #Install FreeOfficeAppImage 
		                echo "================ Установка FreeOfficeAppImage ================"
			        wget https://www.dropbox.com/sh/pfwx54yeguqufpn/AABnFK9WNOUWlu_JEMwpC_Eca?dl=0 && \
				unzip -q 'AABnFK9WNOUWlu_JEMwpC_Eca?dl=0'
                                chmod +x FreeOffice/FreeOfficeApp.desktop
                                cp FreeOffice/FreeOfficeApp.desktop ~/.local/share/applications/
                                sudo cp FreeOffice/FreeOffice.png /usr/share/icons/
				chmod +x ./FreeOffice/*.AppImage
				sudo mv FreeOffice/FreeOffice*.AppImage /opt/FreeOffice.AppImage
				cp FreeOffice/FreeOfficeUninstall.sh ~/'Рабочий стол'/Uninstall 
                                rm -rf 'AABnFK9WNOUWlu_JEMwpC_Eca?dl=0' && rm -Rf FreeOffice                                 			 	                              
                               ;;
                          94)
			        #Install OnlyOfficeAppImage
				echo "================ Установка OnlyOfficeAppImage ================"
				wget https://www.dropbox.com/sh/ptl3svv61irikb2/AADaNs8m9QVSPup3vrmbMfeVa?dl=0 && \
				unzip -q 'AADaNs8m9QVSPup3vrmbMfeVa?dl=0'
                                chmod +x OnlyOffice/OnlyOfficeApp.desktop
                                cp OnlyOffice/OnlyOfficeApp.desktop ~/.local/share/applications/
                                sudo cp OnlyOffice/Onlyoffice.png /usr/share/icons/
				chmod +x ./OnlyOffice/*.AppImage
				sudo mv OnlyOffice/DesktopEditors*.AppImage /opt/DesktopEditors.AppImage
				cp OnlyOffice/OnlyOfficeUninstall.sh ~/'Рабочий стол'/Uninstall 
                                rm -rf 'AADaNs8m9QVSPup3vrmbMfeVa?dl=0' && rm -Rf OnlyOffice                                                                                
                               ;;
                          95)
		               #Install WPSOfficeAppImage 
		                echo "================ Установка WPSOfficeAppImage ================"
			        wget https://www.dropbox.com/sh/8uf25m1pti1ud3o/AACxBdMG6yX8F6nkchp4zfa-a?dl=0 && \
				unzip -q 'AACxBdMG6yX8F6nkchp4zfa-a?dl=0'
                                chmod +x WPSOffice/WPS-Office.desktop
                                cp WPSOffice/WPS-Office.desktop ~/.local/share/applications/
                                sudo cp WPSOffice/WpsOffice.png /usr/share/icons/
				chmod +x ./WPSOffice/*.AppImage
				sudo mv WPSOffice/WPS-Office*.AppImage /opt/WPS-Office.AppImage
				cp WPSOffice/WPSOfficeUninstall.sh ~/'Рабочий стол'/Uninstall 
                                rm -rf 'AACxBdMG6yX8F6nkchp4zfa-a?dl=0' && rm -Rf WPSOffice                         
                               ;;
                          96)
			        #Install ConkyAll 
			        echo "================ Установка ConkyAll ================"
                                sudo apt install -y conky-all
				wget https://www.dropbox.com/sh/fp9f9ek4cuecpfe/AABNfOcmlk0YnezVWxTjTN2Sa?dl=0 && \
				unzip -q 'AABNfOcmlk0YnezVWxTjTN2Sa?dl=0'
				tar xvf Conky/GreenDark.tar.gz
				sudo mv auzia-conky /opt/auzia-conky
				tar xvf Conky/scripts.tar.gz
				sudo mv scripts /opt/scripts
				tar xvf Conky/start_conky.desktop.tar.gz
				chmod +x start_conky.desktop
				sudo mv start_conky.desktop /etc/xdg/autostart/				
				chmod +x Conky/conky.desktop
                                sudo cp Conky/conky.desktop /etc/xdg/autostart/		                                 
				tar xvf Conky/GrannySmithApple.tar.gz
                                sudo cp -r conky /etc/ && rm -Rf conky
				cp Conky/ConkyUninstall.sh ~/'Рабочий стол'/Uninstall 
                                rm -rf 'AABNfOcmlk0YnezVWxTjTN2Sa?dl=0' && rm -Rf Conky
                               ;;
                          97)
			        #Install GnomePie
			        echo "================ Установка GnomePie ================"
			        sudo apt install -y gnome-pie
			        wget https://www.dropbox.com/sh/wtctfwmzlxoe9u7/AADDOkDbMEn-Cm96_e9w3wBGa?dl=0 && \
				unzip -q 'AADDOkDbMEn-Cm96_e9w3wBGa?dl=0'			        
				tar xvf GnomePie/gnome-pie.tar.gz
				mv gnome-pie ~/.config/gnome-pie
				cp GnomePie/GnomePieUninstall.sh ~/'Рабочий стол'/Uninstall 
			        rm -rf 'AADDOkDbMEn-Cm96_e9w3wBGa?dl=0' && rm -Rf GnomePie 	                                                       
                               ;;
                          98)
			        #Install Plank
			        echo "================ Установка Plank ================"
			        sudo apt install -y plank
				wget https://www.dropbox.com/sh/wtctfwmzlxoe9u7/AADDOkDbMEn-Cm96_e9w3wBGa?dl=0 && \
				unzip -q 'AADDOkDbMEn-Cm96_e9w3wBGa?dl=0'			        
				tar xvf GnomePie/plank.tar.gz
				mv plank ~/.config/plank
				cp GnomePie/plank.desktop ~/.config/autostart/
				cp GnomePie/PlankUninstall.sh ~/'Рабочий стол'/Uninstall 
			        rm -rf 'AADDOkDbMEn-Cm96_e9w3wBGa?dl=0' && rm -Rf GnomePie                                                                
                               ;;
                          99)				 
			        #Install Cairo-dock
			        echo "================ Установка Cairo-dock ================"
                                sudo apt install -y cairo-dock
				wget https://www.dropbox.com/sh/so0lexf3aft7diz/AAAJnuUJJa7cUQUIUk2flPwma?dl=0 && \
				unzip -q 'AAAJnuUJJa7cUQUIUk2flPwma?dl=0'
                                chmod +x CairoDock/cairo-dock.desktop
                                cp CairoDock/cairo-dock.desktop ~/.config/autostart/                                 
				tar xvf CairoDock/Bcairo-dock.tar.gz
                                mv cairo-dock ~/.config/cairo-dock
				cp CairoDock/Cairo-dockUninstall.sh ~/'Рабочий стол'/Uninstall 
                                rm -rf 'AAAJnuUJJa7cUQUIUk2flPwma?dl=0' && rm -Rf CairoDock		                              
                               ;;
                        100)
			        #Install Речевой Сервер Speech Dispatcher
				echo "================ Установка Речевой Сервер Speech Dispatcher ================"
				sudo apt install -y speech-dispatcher speech-dispatcher-audio-plugins speech-dispatcher-espeak-ng
                                sudo apt install -y espeak espeak-data espeak-ng-data espeakup libespeak-ng1 libespeak1 orca                                
				wget https://www.dropbox.com/sh/61eox35lnu7m7es/AAAG_jvUgmCiIEe9LlnWFblPa?dl=0 && \
				unzip -q 'AAAG_jvUgmCiIEe9LlnWFblPa?dl=0'
                                chmod -R 777 Orca
                                sudo cp Orca/espeak.desktop /usr/share/applications/
                                cp Orca/orca-autostart.desktop ~/.config/autostart/ 
                                sudo cp Orca/Espeak.png /usr/share/icons/
                                sudo cp Orca/orca.png /usr/share/icons/
				cp Orca/SpeechDispatcherUninstall.sh ~/'Рабочий стол'/Uninstall 
                                rm -rf 'AAAG_jvUgmCiIEe9LlnWFblPa?dl=0' && rm -Rf Orca                             				                              
                               ;;
                        101)
			        #Install Из каталога deb*
		                echo "================ Установка Из каталога deb ======================"
		                cd deb/
		                sudo dpkg -i *.deb
		                sudo apt install -f -y
		                cd ../


	    esac
	done

echo "Запуск обновления плагинов"
sleep 1s
sudo apt install -y xfce4-goodies xfce4-battery-plugin xfce4-clipman xfce4-clipman-plugin \
xfce4-cpufreq-plugin xfce4-datetime-plugin xfce4-diskperf-plugin xfce4-fsguard-plugin \
xfce4-genmon-plugin xfce4-mount-plugin xfce4-xkb-plugin xfce4-sensors-plugin \
xfce4-smartbookmark-plugin xfce4-timer-plugin xfce4-wavelan-plugin \
xfce4-power-manager-plugins xfce4-pulseaudio-plugin

#Установка программ из репозиториев.
echo "Начало установки программ из репозиториев."
sleep 1s
sudo apt install -y gparted menulibre uget vlc vlc-l10n git grub-customizer \
gnome-disk-utility synaptic   
echo 'Установка дополнительных программ по мере изучения Linux'
sudo apt install -y tumbler mtpaint thunar-volman hardinfo lshw lshw-gtk \
seahorse psensor gtkhash thunar-gtkhash celluloid mpv winff preload \
neofetch dbus-x11 partitionmanager htop libfuse2 android-file-transfer  
#PulseAudioAlsa
#echo 'Устанавливаем звук от  pulseaudio'  #https://www.freedesktop.org/wiki/Software/PulseAudio/
#sudo apt install -y pulseaudio gstreamer1.0-pulseaudio pulseaudio-utils xfce4-pulseaudio-plugin \
#pavucontrol libpulse-mainloop-glib0 libpulse0 libpulsedsp
#echo 'Устанавливаем звук от Alsa'   #https://alsa-project.org/wiki/Main_Page
#sudo apt install -y alsa-utils gstreamer1.0-alsa libasound2 libasound2-data libasound2-plugins
#Установка заголовочных файлов ядра   http://rus-linux.net/MyLDP/kernel/kernel-headers.html
sudo apt install -y linux-headers-$(uname -r|sed 's,[^-]*-[^-]*-,,')
#Установка настроек внешнего вида
#Hv3WebBrowser-Applications  
wget https://www.dropbox.com/sh/hw7poa8emh7861n/AADZ3FErDJ3PWgBZfE-qq_uXa?dl=0 && \
unzip -q 'AADZ3FErDJ3PWgBZfE-qq_uXa?dl=0'
chmod +x Browser/hv3.desktop
cp Browser/hv3.desktop ~/.local/share/applications/ 
sudo mv Browser/Hv3WebBrowser.png /usr/share/icons/
cp Browser/Hv3WebBrowserUninstall.sh ~/'Рабочий стол'/Uninstall
rm -rf 'AADZ3FErDJ3PWgBZfE-qq_uXa?dl=0' && rm -Rf Browser
#OptSh
wget https://www.dropbox.com/sh/htkroepykcsnzuj/AACRUtzQHLAzV0LhWDKl9oSfa?dl=0 && \
unzip -q 'AACRUtzQHLAzV0LhWDKl9oSfa?dl=0'
chmod -R 777 OptSh
sudo mv OptSh /opt
sudo chown -R $USER:$USER /opt/OptSh/
#OptApl
chmod -R 777 OptApl
rm OptApl/Drivers.desktop                       
rm OptApl/Moc.desktop                           
rm OptApl/MouseRestart.desktop                  
rm OptApl/АвтоматическийРежим.desktop                    
rm OptApl/КонсервативныйРежим.desktop             
rm OptApl/МаксимальноеБыстродействие.desktop                
rm OptApl/SourcesListGedit.desktop
rm OptApl/SourcesListLeafpad.desktop
rm OptApl/SourcesListMousepad.desktop
rm OptApl/software-properties-drivers.desktop   
rm OptApl/SynapticUpgrade.desktop               
rm OptApl/Энергосбережение.desktop
rm OptApl/system-upgrade.desktop
mv OptApl ~/.local/share/applications/OptApl
sudo chown -R $USER:$USER ~/.local/share/applications/OptApl
#OptPng
chmod -R 777 OptPng
sudo mv OptPng /usr/share/icons/OptPng
sudo chown -R $USER:$USER /usr/share/icons/OptPng
mv OptShAplPngUninstall.sh ~/'Рабочий стол'/Uninstall
rm -rf 'AACRUtzQHLAzV0LhWDKl9oSfa?dl=0'
#Install Prelink для ускорения запуска программ в Linux			 
wget https://www.dropbox.com/sh/3zaw6kt99g2rstx/AACFXjA4sG9raoxhU8zSxi8Wa?dl=0 && \
unzip -q 'AACFXjA4sG9raoxhU8zSxi8Wa?dl=0'
sudo dpkg -i Prelink/*.deb
sudo apt install -fy
rm -rf 'AACFXjA4sG9raoxhU8zSxi8Wa?dl=0' && rm -Rf Prelink
#LibreofficeAplPng
wget https://www.dropbox.com/sh/5b4vlk0phzsvi3f/AAA1kT9j_a1_tOSTp4jDPR9ta?dl=0 && \
unzip -q 'AAA1kT9j_a1_tOSTp4jDPR9ta?dl=0'
chmod +x LibreOffice/*.desktop                                    
cp LibreOffice/libreoffice-draw.desktop ~/.local/share/applications/    
cp LibreOffice/libreoffice-math.desktop ~/.local/share/applications/         
cp LibreOffice/libreoffice-writer.desktop ~/.local/share/applications/
cp LibreOffice/libreoffice-calc.desktop ~/.local/share/applications/ 
cp LibreOffice/libreoffice-impress.desktop ~/.local/share/applications/  
cp LibreOffice/libreoffice-startcenter.desktop ~/.local/share/applications/
cp LibreOffice/LibreofficeUninstall.sh ~/'Рабочий стол'/Uninstall 
chmod -R 777 LibreOffice/LibreOffice			
sudo cp -R LibreOffice/LibreOffice /usr/share/icons/
rm -rf 'AAA1kT9j_a1_tOSTp4jDPR9ta?dl=0' && rm -Rf LibreOffice
#Install MousepadAplPng
sudo apt install -y mousepad
wget https://www.dropbox.com/sh/3sp3cqvbs8i6b4n/AAAXqoYoz8YaC_YakjxJaRAWa?dl=0 && \
unzip -q 'AAAXqoYoz8YaC_YakjxJaRAWa?dl=0'
chmod +x Mousepad/mousepad.desktop
cp Mousepad/mousepad.desktop ~/.local/share/applications/
sudo cp Mousepad/Mousepad.png /usr/share/icons/
cp Mousepad/MousepadUninstall.sh ~/'Рабочий стол'/Uninstall   
rm -rf 'AAAXqoYoz8YaC_YakjxJaRAWa?dl=0' && rm -Rf Mousepad
#Install Geany
sudo apt install -y geany
wget https://www.dropbox.com/sh/cu7gouwyz2ovbf5/AACkbpyqvUmq9EckFzgPkc_Ka?dl=0 && \
unzip -q 'AACkbpyqvUmq9EckFzgPkc_Ka?dl=0'
chmod +x Geany/geany.desktop
cp Geany/geany.desktop ~/.local/share/applications/
sudo cp Geany/Geany.png /usr/share/icons
tar xvf Geany/geany.tar.gz
mv geany ~/.config/geany
cp Geany/GeanyUninstall.sh ~/'Рабочий стол'/Uninstall
rm -rf 'AACkbpyqvUmq9EckFzgPkc_Ka?dl=0' && rm -Rf Geany
#Install FirefoxESR
sudo apt install -y firefox-esr firefox-esr-l10n-ru
wget https://www.dropbox.com/sh/5fffc86mpq4dxua/AABEUmmp8wULRoKP2yanxTRka?dl=0 && \
unzip -q 'AABEUmmp8wULRoKP2yanxTRka?dl=0'
chmod +x Firefox/firefox-esr.desktop
cp Firefox/firefox-esr.desktop ~/.local/share/applications/
sudo cp Firefox/Firefox.svg /usr/share/icons/
tar xvf Firefox/mozilla.tar.gz
cp Firefox/FirefoxUninstall.sh ~/'Рабочий стол'/Uninstall 
rm -rf 'AABEUmmp8wULRoKP2yanxTRka?dl=0' && rm -Rf Firefox
#ПрозрачныйФонIcons
wget https://www.dropbox.com/sh/61u0gag8i9au1m1/AAAzprmaVblT-j44jUnNtgVAa?dl=0 && \
unzip -q 'AAAzprmaVblT-j44jUnNtgVAa?dl=0'
tar xvf РабочийСтол/gtkrc-2.0.tar.gz
#Рабочий стол и дополнение
cp РабочийСтол/Компьютер.desktop ~/'Рабочий стол'/
cp РабочийСтол/КтоИспользуетDebian.sh ~/'Рабочий стол'/
cp РабочийСтол/ПаролиLiveссылки.ctb.pdf ~/'Рабочий стол'/
rm -rf 'AAAzprmaVblT-j44jUnNtgVAa?dl=0' && rm -Rf РабочийСтол
#Install KlichalexWeb
wget https://www.dropbox.com/sh/fj0l3tn6crbsfyg/AABqfHbT3o40SvyfD9yprFD-a?dl=0 && \
unzip -q 'AABqfHbT3o40SvyfD9yprFD-a?dl=0'
chmod +x GTSSS/KlichalexF.desktop
cp GTSSS/KlichalexF.desktop ~/'Рабочий стол'/ 
cp GTSSS/KlichalexF.desktop ~/.local/share/applications/
sudo cp GTSSS/Klichalex.jpg /usr/share/icons/
cp GTSSS/KlichalexWebUninstall.sh ~/'Рабочий стол'/Uninstall  
rm -rf 'AABqfHbT3o40SvyfD9yprFD-a?dl=0' && rm -Rf GTSSS
#Install UserInstalledPackages
wget https://www.dropbox.com/sh/q9umcsqs866sv8l/AACD2TXDiZ--vmanc23_xOeGa?dl=0 && \
unzip -q 'AACD2TXDiZ--vmanc23_xOeGa?dl=0'                              
cp UIP/UserInstalledPackages.png ~/'Рабочий стол'/
cp UIP/UserInstalledPackagesUninstall.sh ~/'Рабочий стол'/Uninstall 
rm -rf 'AACD2TXDiZ--vmanc23_xOeGa?dl=0' && rm -Rf UIP
#Внешний Вид
cp -r appearance/xfce4 ~/.config/
cp -r appearance/panel ~/.config/
#прописываем владельца каталога xfce4
chown -R ${name}:${name} ~/.config/xfce4/
chown -R ${name}:${name} ~/.config/panel/
#удаление папки appearance
rm -Rf appearance

echo "Давайте настроим автоматический запуск системы."
echo "(Если Вы, конечно, это хотите)"
echo "===================================="
echo ""
echo ""
echo "Автоматический вход в Xfce настраивается путем редактирования файла «/etc/lightdm/lightdm.conf»."
echo "Для этого необходимо найти в пункте [Seat:*] две строки :"
echo "#autologin-user="
echo "#autologin-user-timeout=0"
echo "И изменить их, раскоментировать и прописать свой логин, например:"
echo "autologin-user=alex  #вместо alex Ваш логин"
echo "autologin-user-timeout=0"
echo "Сохранить изменения конфига"
echo "Запустить lightdm.conf для редактирования?"
echo "y - запустить, любой другой символ - нет"
read doing 
case $doing in
y)
 sudo geany /etc/lightdm/lightdm.conf
 ;;
*)
 echo 'Редактирование lightdm.conf отменено'
esac #окончание оператора case.
echo ""
echo ""

echo 'Обновление и очистка после всех установок'
sudo apt full-upgrade -y
sudo apt autoremove -y
#очищает папку /var/cache/apt/archives;
sudo aptitude clean -y
#Очистка APT кеш 
sudo apt clean -y

echo "Скрипт работу закончил."
echo "Спасибо klichalex автору скрипта Transformation! https://www.youtube.com/user/klichalex/featured"
echo "Спасибо klichalex автору хороших видео уроков! https://rutube.ru/channel/23628980/videos/"
echo "Спасибо klichalex автору перевода Refracta на Русский язык http://prostolinux.my1.ru/"
echo "Спасибо circulosmeos https://github.com/circulosmeos за предоставленые скрипты"
echo "Спасибо разработчику скриптов Dmitriy wj42ftns Chekhov, Russia https://gist.github.com/wj42ftns"
echo "Спасибо kachnu https://github.com/kachnu за предоставленые скрипты"
echo "Спасибо Kendek https://gitlab.com/Kendek/systemback"
echo "Спасибо LeCorbeau's Vault https://lecorbeausvault.wordpress.com/2021/01/10/quickly-build-a-custom-bootable-installable-debian-live-iso-with-live-build/"
echo "Спасибо Франко Кониди https://syslinuxos.com/things-to-do-after-installing-syslinuxos-12/"
echo "Не важно насколько медленно ты движешься, главное не останавливаться.Конфуций"
echo "Наш ответ Чемберлену!Дави Империализма Гиену Могучий Рабочий Класс!" 
echo "Вчера были танки лишь у Чемберлена,А нынче есть и у нас!"
echo "Запомните,чтобы ничего не делать, надо уметь делать все" 
echo "Я не потерпел неудачу, я нашел 10 000 способов, которые не сработают-Эдисон."
echo "Операция настройки системы завершена"
echo "Выйти из настроек, или перезапустить систему?"
echo "y - выйти, любой другой символ - перезапуск"
read doing 
case $doing in
y)
  exit
 ;;
*)
sudo reboot -f
esac #окончание оператора case.
